#include "App.h"
#include "utils.h"

#include <iostream>
#include <vector>


#define LIGHT_COUNT 19

BaseApp* createApp(void)
{
    return new App();
}

void releaseApp(BaseApp * pApp)
{
    delete pApp;
}

void App::moveCamera(const float3 &dir)
{
    camPos = camPos + dir * (camSpeed * frameTime);
}

void App::resetCamera()
{
    camPos = vec3(-730, 20, 2010);
    camRotY = 0.14f;
    camRotX = -2.63f;
}

void App::onSize(const int w, const int h)
{
    D3D11App::onSize(w, h);

    if (renderer)
    {
        renderer->resizeRenderTarget(m_BaseRT, w, h, 1, 1, 1);
        renderer->resizeRenderTarget(m_NormalRT, w, h, 1, 1, 1);
        renderer->resizeRenderTarget(m_DepthRT, w, h, 1, 1, 1);
        renderer->resizeRenderTarget(m_DepthUserRT, w, h, 1, 1, 1);
        renderer->resizeRenderTarget(m_TempRT, w, h, 1, 1, 1);
        renderer->resizeRenderTarget(m_TempRT2, w, h, 1, 1, 1);
        renderer->resizeRenderTarget(m_lightedSceneRT, w, h, 1, 1, 1);
        renderer->resizeRenderTarget(m_HalfScreenRT, w / downSampleRate, h / downSampleRate, 1, 1, 1);
    }
}

bool App::onKey(const uint key, const bool pressed) {
    return BaseApp::onKey(key, pressed);
}

void App::createScreenPostprocessQuad() {
    m_screenQuad = new Model();

    float hw = 1; // half width
    float hh = 1; // half height
    m_screenQuad->createQuad(float3(-hw, -hh, 0),   // top left vertex
                             float3(hw, -hh, 0),    // top right
                             float3(hw, hh, 0),     // bottom right
                             float3(-hw, hh, 0));   // bottom left
    m_screenQuad->makeDrawable(renderer, true, m_�onvolution1D);
}

void App::initEffects() {
    occlusionShafts = new LightShaftOcclusionEffect(renderer, this);
    occlusionShafts->init();

    bloomShafts = new LightShaftBloomEffect(renderer, this);
    bloomShafts->init();
}

bool App::init()
{
    depthBits = 0;
    initAppGUI();
    initLights();

    return true;
}

bool App::loadModels() {
    using utils::ModeFastInit;

    /* Convenient way to load models declaratively */
    ModeFastInit modelPaths[] = {
        ModeFastInit(&m_Map, "Models/Corridor2/Map.obj", float3(1, 1, -1)),
        ModeFastInit(&m_DinamicObject, "Models/Cube.obj"),
        ModeFastInit(&m_Chest, "Models/Chest.obj", float3(2.5, 2.5, 2.5)),
        ModeFastInit(&m_Frustem, "Models/LightShaftFrustem.obj")
    };

    for (ModeFastInit& mfi : modelPaths) {
        Model *pModel = *mfi.ppModel = new Model;
        if (!pModel->loadObj(mfi.path.c_str())) {
            return false;
        }
        pModel->scale(0, mfi.scale);
        pModel->computeTangentSpace(true);
    }
    

    /* Load / create other models */
    m_lightBoundingSphere = new Model();
    m_lightBoundingSphere->createSphere(3);

    return true;
}

bool App::initLights()
{
    m_LightSystem.setShaderPath("Shaders/Lighting.shd");
    m_LightSystem.setFullMode(false);

    // first go lights close to camera init position
    lightShaftsLight = new PointLight(float3(-192.0f, 96.0f, 1792.0f), 550.0f);
    lightShaftsLightId = m_LightSystem.addSource(lightShaftsLight, true);

    m_LightSystem.addSource(new PointLight(float3(-832.0f, 96.0f, 1792.0f), 550.0f), true);
    m_LightSystem.addSource(new PointLight(float3(1792.0f, 96.0f, 320.0f), 550.0f), true);
    m_LightSystem.addSource(new PointLight(float3(1792.0f, 96.0f, -320.0f), 550.0f), true);
    m_LightSystem.addSource(new PointLight(float3(960.0f, 32.0f, -640.0f), 450.0f), true);
    m_LightSystem.addSource(new PointLight(float3(-640.0f, 32.0f, -960.0f), 450.0f), true);
    m_LightSystem.addSource(new PointLight(float3(-960.0f, 32.0f, 640.0f), 450.0f), true);
    m_LightSystem.addSource(new PointLight(float3(640.0f, 32.0f, 960.0f), 450.0f), true);

    m_LightSystem.addSource(new PointLight(float3(576.0f, 96.0f, 0.0f), 640.0f), false);
    m_LightSystem.addSource(new PointLight(float3(0.0f, 96.0f, 576.0f), 640.0f), false);
    m_LightSystem.addSource(new PointLight(float3(-576.0f, 96.0f, 0.0f), 640.0f), false);
    m_LightSystem.addSource(new PointLight(float3(0.0f, 96.0f, -576.0f), 640.0f), false);
    m_LightSystem.addSource(new PointLight(float3(1280.0f, 32.0f, 192.0f), 450.0f), false);
    m_LightSystem.addSource(new PointLight(float3(1280.0f, 32.0f, -192.0f), 450.0f), false);
    m_LightSystem.addSource(new PointLight(float3(-320.0f, 32.0f, 1280.0f), 450.0f), false);
    m_LightSystem.addSource(new PointLight(float3(-704.0f, 32.0f, 1280.0f), 450.0f), false);
    m_LightSystem.addSource(new PointLight(float3(960.0f, 32.0f, 640.0f), 450.0f), false);
    m_LightSystem.addSource(new PointLight(float3(-960.0f, 32.0f, -640.0f), 450.0f), false);
    m_LightSystem.addSource(new PointLight(float3(640.0f, 32.0f, -960.0f), 450.0f), false);

    return true;
}

// TODO: Victor, refactor...
void App::initDinamicObjects(uint numberOfGroups, uint numberOfObjectsInGroup) {
    m_DinamicObjectInstance = new DinamicModelInstance**[numberOfGroups];
    m_ChestInstance = new DinamicModelInstance*[numberOfGroups];
    m_FrustemInstance = new DinamicModelInstance*[numberOfGroups];

    float chestRotatesAngles[] = {
        M_PI_DIV_2 * 3.0f, //0L
        M_PI_DIV_2 * 0.0f, //1L
        M_PI_DIV_2 * 1.0f, //2L
        M_PI_DIV_2 * 2.0f, //3L
        M_PI_DIV_2 * 1.5f, //4L
        M_PI_DIV_2 * 0.5f, //5L
        M_PI_DIV_2 * 1.0f, //6L
        M_PI_DIV_2 * 3.0f, //7L
        M_PI_DIV_2 * 2.0f, //8L
        M_PI_DIV_2 * 0.0f, //9L
        M_PI_DIV_2 * 1.0f, //10L
        M_PI_DIV_2 * 3.0f, //11L
        M_PI_DIV_2 * 1.0f, //12L
        M_PI_DIV_2 * 1.0f, //13L
        M_PI_DIV_2 * 0.0f, //14L
        M_PI_DIV_2 * 0.0f, //15L
        M_PI_DIV_2 * 3.0f, //16L
        M_PI_DIV_2 * 3.0f, //17L
        M_PI_DIV_2 * 2.0f, //18L
    };

    for (uint i = 0; i < numberOfGroups; i++) {
        m_DinamicObjectInstance[i] = new  DinamicModelInstance*[numberOfObjectsInGroup];
        for (uint j = 0; j < numberOfObjectsInGroup; j++) {
            m_DinamicObjectInstance[i][j] = new DinamicModelInstance(m_DinamicObject, vec3(m_LightSystem.getLightPos(i)));
        }
        m_ChestInstance[i] = new DinamicModelInstance(m_Chest, vec3(m_LightSystem.getLightPos(i).x, -200.f, m_LightSystem.getLightPos(i).z), 0.f, chestRotatesAngles[i]);

        m_FrustemInstance[i] = new DinamicModelInstance(m_Frustem, vec3(m_LightSystem.getLightPos(i).x, -200.f, m_LightSystem.getLightPos(i).z), 0.f, chestRotatesAngles[i]);
    }

    m_numberOfGroups = numberOfGroups;
    m_numberOfObjectsInGroup = numberOfObjectsInGroup;
}

void App::initAppGUI() {
    float x = 20, y = 50, w = 200, h = 30, dy = h + 5; // widgets x, y, width, height and margin

    /* Upper common menu */
    float MAX_RADIUS = 1000.0f;
    addWidget(m_LightRadiusSlider = new Slider(x, y += 0, 200, h, 0, MAX_RADIUS, m_lightsRadius));
    addWidget(m_zBufferInversedCheckbox = new CheckBox(x, y += dy, 300, h, "Inversed z-Buffer", m_zBufferIsInversed));
    addWidget(m_enablePostProcessCB = new CheckBox(x, y += dy, 300, h, "Enable post process", m_postProcessEnabled));
    addWidget(m_showDepthBufferCB = new CheckBox(x, y += dy, 300, h, "Show z-Buffer", m_showDepthBuffer));

    // Light shaft mode dropdown list switcher
    addWidget(lightShaftModeDropdown = new DropDownList(x, y += dy, 300, h));
    // map returned dropdown item names to PostprocesMode enumeration
    itemIdToMode.emplace(lightShaftModeDropdown->addItem("No shafts"), NO_POST_PROCESS);
    itemIdToMode.emplace(lightShaftModeDropdown->addItem("Occlusion"), LIGHT_SHAFT_OCCLUSION);
    itemIdToMode.emplace(lightShaftModeDropdown->addItem("Bloom"), LIGHT_SHAFT_BLOOM);
    itemIdToMode.emplace(lightShaftModeDropdown->addItem("Geometric"), LIGHT_SHAFT_GEOMETRIC);


    float settingsY = y + dy;

    /* LSBloom Settings */
    addWidget(lsbSettingsGroup = new Dialog(x, settingsY, w + 150, 10 * dy, false, false, false, false));

    float innerX = 5, innerY = 5;
    lsbSettingsSliders.push_back(new Slider("diskRad", innerX, innerY += 0, w, h, 0, 50, lsbSettings.diskRadius));
    lsbSettingsSliders.push_back(new Slider("exp    ", innerX, innerY += dy, w, h, 0, 1, lsbSettings.exposure));
    lsbSettingsSliders.push_back(new Slider("decay  ", innerX, innerY += dy, w, h, 0.9f, 1, lsbSettings.decay));
    lsbSettingsSliders.push_back(new Slider("density", innerX, innerY += dy, w, h, 0.5f, 1, lsbSettings.density));
    lsbSettingsSliders.push_back(new Slider("weight ", innerX, innerY += dy, w, h, 0, 1, lsbSettings.weight));
    lsbSettingsSliders.push_back(new Slider("samples", innerX, innerY += dy, w, h, 0, 140, static_cast<float>(lsbSettings.numSamples)));

    lsbSettingsGroup->addTab("LS Bloom Settings");
    for (auto slider : lsbSettingsSliders) {
        lsbSettingsGroup->addWidget(0, slider);
        slider->setListener(this);
    }
    lsbSettingsGroup->addWidget(0, lsbShowBloomCB = new CheckBox(innerX, innerY += dy, w, h, "Show only bloom", lsbSettings.showOnlyBloom));
    lsbShowBloomCB->setListener(this);
    lsbSettingsGroup->addWidget(0, resetLSBButton = new Button(innerX, innerY += dy, w, h, "Reset"));
    resetLSBButton->setListener(this);

    lsbSettingsGroup->fitHeight();


    /* LSOcclusion Settings */
    innerX = 5, innerY = 5;
    addWidget(lsoSettingsGroup = new Dialog(x, settingsY, w + 150, 10 * dy, false, false, false, false));

    lsoSettingsSliders.push_back(new Slider("Window", innerX, innerY += 0, w, h, 20.0f, 200.0f, lsoSettings.samples));
    lsoSettingsSliders.push_back(new Slider("Samples", innerX, innerY += dy, w, h, 5.0f, 70.0f, lsoSettings.samples));
    lsoSettingsSliders.push_back(new Slider("Factor", innerX, innerY += dy, w, h, 0.1f, 1.0f, lsoSettings.factor));
    lsoSettingsSliders.push_back(new Slider("Bright", innerX, innerY += dy, w, h, 0.1f, 12.0f, lsoSettings.bright));
    lsoSettingsSliders.push_back(new Slider("fColor", innerX, innerY += dy, w, h, 0.0f, 1.0f, lsoSettings.fogColor));
    lsoSettingsSliders.push_back(new Slider("fDensity", innerX, innerY += dy, w, h, 0.0f, 1.0f, lsoSettings.fogDensity));

    lsoSettingsGroup->addTab("LS Occlusion Settings");
    for (auto slider : lsoSettingsSliders) {
        lsoSettingsGroup->addWidget(0, slider);
        slider->setListener(this);
    }
    lsoSettingsGroup->addWidget(0, lsoDepthBufferCB = new CheckBox(innerX, innerY += dy, w, h, "Depth Buffer", lsoSettings.blurBufffer));
    lsoDepthBufferCB->setListener(this);
    lsoSettingsGroup->addWidget(0, resetLSOButton = new Button(innerX, innerY += dy, w, h, "Reset"));
    resetLSOButton->setListener(this);

    lsoSettingsGroup->fitHeight();


    /* LSGeometry Settings */
    innerX = 5, innerY = 5;
    addWidget(lsgSettingsGroup = new Dialog(x, settingsY, w + 150, 10 * dy, false, false, false, false));

    lsgSettingsSliders.push_back(new Slider("RayCount", innerX, innerY += 0,  w, h, 11,   31,   float(lsgSettings.rayCount)));
    lsgSettingsSliders.push_back(new Slider("Period",   innerX, innerY += dy, w, h, 0.1f, 1.0f, lsgSettings.period));
    lsgSettingsSliders.push_back(new Slider("Speed",    innerX, innerY += dy, w, h, 0.5f, 7.0f, lsgSettings.speed));
    lsgSettingsSliders.push_back(new Slider("High",     innerX, innerY += dy, w, h, 0.0f, 1.0f, lsgSettings.high));

    lsgSettingsGroup->addTab("LS Geometry Settings");
    for (auto slider : lsgSettingsSliders) {
        lsgSettingsGroup->addWidget(0, slider);
        slider->setListener(this);
    }
    lsgSettingsGroup->addWidget(0, resetLSGButton = new Button(innerX, innerY += dy, w, h, "Reset"));
    resetLSGButton->setListener(this);

    lsgSettingsGroup->fitHeight();


    lightShaftModeDropdown->selectItem(postprocessMode);

    m_HelpDisplayTime = 6;
}

void App::updateSettingsVisibility()
{
    lsbSettingsGroup->setVisible(postprocessMode == LIGHT_SHAFT_BLOOM);
    lsoSettingsGroup->setVisible(postprocessMode == LIGHT_SHAFT_OCCLUSION);
    lsgSettingsGroup->setVisible(postprocessMode == LIGHT_SHAFT_GEOMETRIC);
}

void App::exit()
{
    delete m_Map;
    delete m_lightBoundingSphere;
    delete m_screenQuad;

    delete m_DinamicObject;
    delete m_Chest;
    delete m_Frustem;

    for (uint i = 0; i < m_numberOfGroups; i++) {
        for (uint j = 0; j < m_numberOfObjectsInGroup; j++) {
            delete m_DinamicObjectInstance[i][j];
        }
        delete m_DinamicObjectInstance[i];
        delete m_ChestInstance[i];
        delete m_FrustemInstance[i];
    }
    delete m_DinamicObjectInstance;
    delete m_ChestInstance;
    delete m_FrustemInstance;

    delete occlusionShafts;
    delete bloomShafts;
}

void App::loadUserConfig()
{
    try {
        LSBSettings* lsb_settings = reinterpret_cast<LSBSettings*>(config.getObject("LSBSettings"));
        if (lsb_settings) lsbSettings = *lsb_settings;
    }
    catch (...) {
        std::cerr << "Could not load LSBloom config parameters" << std::endl;
    }

    try {
        LSOSettings* lso_settings = reinterpret_cast<LSOSettings*>(config.getObject("LSOSettings"));
        if (lso_settings) lsoSettings = *lso_settings;
    }
    catch (...) {
        std::cerr << "Could not load LSOcclusion config parameters" << std::endl;
    }

}

void App::updateUserConfig() {
    config.setObject("LSBSettings", lsbSettings);
    config.setObject("LSOSettings", lsoSettings);
}

bool App::initAPI()
{
    return D3D11App::initAPI(D3D11, DXGI_FORMAT_R8G8B8A8_UNORM, DXGI_FORMAT_UNKNOWN, 1, NO_SETTING_CHANGE);
}

bool App::load()
{
    // Shaders
    if ((m_FillBuffers = renderer->addShader("Shaders/FillBuffers.shd")) == SHADER_NONE) return false;
    if ((m_GeometricLightShafts = renderer->addShader("Shaders/GeometricLightShafts.shd")) == SHADER_NONE) return false;
    if ((m_Lighting = renderer->addShader(m_LightSystem.getShaderPath())) == SHADER_NONE) return false;
    if ((m_ZBufferNormalized = renderer->addShader("Shaders/DepthLinear.shd")) == SHADER_NONE) return false;
    if ((m_RenderDownsampled = renderer->addShader("Shaders/RenderDownsampled.shd")) == SHADER_NONE) return false;
    if ((m_�onvolution1D = renderer->addShader("Shaders/Convolution1D.shd")) == SHADER_NONE) return false;


    // Sampler states
    if ((m_BaseFilter = renderer->addSamplerState(TRILINEAR_ANISO, WRAP, WRAP, WRAP)) == SS_NONE) return false;
    if ((m_PointClamp = renderer->addSamplerState(NEAREST, CLAMP, CLAMP, CLAMP)) == SS_NONE) return false;

    // Main render targets
    if ((m_BaseRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE)) == TEXTURE_NONE) return false;
    if ((m_NormalRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8S, 1, SS_NONE)) == TEXTURE_NONE) return false;
    if ((m_DepthUserRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_R32F, 1, SS_NONE)) == TEXTURE_NONE) return false;
    if ((m_DepthRT = renderer->addRenderDepth(width, height, 1, FORMAT_D16, 1, SS_NONE, SAMPLE_DEPTH)) == TEXTURE_NONE) return false;
    if ((m_lightedSceneRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE)) == TEXTURE_NONE) return false;
    if ((m_TempRT = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE)) == TEXTURE_NONE) return false;
    if ((m_TempRT2 = renderer->addRenderTarget(width, height, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE)) == TEXTURE_NONE) return false;
    if ((m_HalfScreenRT = renderer->addRenderTarget(width / downSampleRate, height / downSampleRate, 1, 1, 1, FORMAT_RGBA8, 1, SS_NONE)) == TEXTURE_NONE) return false;

    // Textures
    if ((m_BaseTex[0] = renderer->addTexture("Textures/wood.dds", true, m_BaseFilter)) == TEXTURE_NONE) return false;
    if ((m_BumpTex[0] = renderer->addNormalMap("Textures/woodBump.dds", FORMAT_RGBA8S, true, m_BaseFilter)) == TEXTURE_NONE) return false;

    if ((m_BaseTex[1] = renderer->addTexture("Textures/Tx_imp_wall_01_small.dds", true, m_BaseFilter)) == TEXTURE_NONE) return false;
    if ((m_BumpTex[1] = renderer->addNormalMap("Textures/Tx_imp_wall_01Bump.dds", FORMAT_RGBA8S, true, m_BaseFilter)) == TEXTURE_NONE) return false;

    if ((m_BaseTex[2] = renderer->addTexture("Textures/floor_wood_4.dds", true, m_BaseFilter)) == TEXTURE_NONE) return false;
    if ((m_BumpTex[2] = renderer->addNormalMap("Textures/floor_wood_4Bump.dds", FORMAT_RGBA8S, true, m_BaseFilter)) == TEXTURE_NONE) return false;

    if ((m_BaseTex[3] = renderer->addTexture("Textures/floor_wood_3.dds", true, m_BaseFilter)) == TEXTURE_NONE) return false;
    if ((m_BumpTex[3] = renderer->addNormalMap("Textures/floor_wood_3Bump.dds", FORMAT_RGBA8S, true, m_BaseFilter)) == TEXTURE_NONE) return false;

    if ((m_BaseTex[4] = renderer->addTexture("Textures/light2.dds", true, m_BaseFilter)) == TEXTURE_NONE) return false;
    if ((m_BumpTex[4] = renderer->addNormalMap("Textures/light2Bump.dds", FORMAT_RGBA8S, true, m_BaseFilter)) == TEXTURE_NONE) return false;

    // Blending states
    if ((m_BlendAdd = renderer->addBlendState(ONE, ONE)) == BS_NONE) return false;
    if ((m_BlendDecal = renderer->addBlendState(SRC_ALPHA, ONE_MINUS_SRC_ALPHA, ZERO, ONE_MINUS_SRC_ALPHA, BM_ADD, BM_ADD)) == BS_NONE) return false;

    if ((m_DepthTest = renderer->addDepthState(true, true, LEQUAL)) == DS_NONE) return false;
    // for inversed z-buffer
    if ((m_DepthTestInversed = renderer->addDepthState(true, true, GEQUAL)) == DS_NONE) return false;
    m_DepthTestCurrent = m_DepthTest;


    if (!loadModels()) {
        return false;
    }

    // Set models vertex formats according to shader inputs
    std::vector<std::pair<Model*, int>> modelShaders = {
        { m_Map, m_FillBuffers },
        { m_lightBoundingSphere, m_Lighting },
        { m_DinamicObject, m_FillBuffers },
        { m_Chest, m_FillBuffers },
        { m_Frustem, m_GeometricLightShafts }
    };
    for (auto& pair : modelShaders) {
        if (!pair.first->makeDrawable(renderer, true, pair.second)) return false;
    }

    initEffects();
    initDinamicObjects(m_LightSystem.getLightCount(), SPHERES_PER_LIGHT_COUNT);
    createScreenPostprocessQuad();
    setZBufferInversed(m_zBufferIsInversed);

    return true;
}

void App::setZBufferInversed(bool inversed /* = true */) {
    m_zBufferIsInversed = inversed;
    m_zBufferClearColor = inversed ? 0.0f : 1.0f;
    m_DepthTestCurrent = inversed ? m_DepthTestInversed : m_DepthTest;
}

void App::unload() {

}

void App::drawFrame() {
    updateFrame();
    

    float near_ = m_zBufferIsInversed ? FAR_PLANE : NEAR_PLANE;
    float far_ = m_zBufferIsInversed ? NEAR_PLANE : FAR_PLANE;
    float4x4 projection = toD3DProjection(perspectiveMatrixY(1.2f, width, height, near_, far_));
    float4x4 view = rotateXY(-camRotY, -camRotX);
    view.translate(-camPos);
    float4x4 viewProj = projection * view;
    // Pre-scale-bias the matrix so we can use the screen position directly
    float4x4 viewProjInv = (!viewProj) * (translate(-1.0f, 1.0f, 0.0f) * scale(2.0f, -2.0f, 1.0f));


    TextureID bufferRTs[] = { m_BaseRT, m_NormalRT, m_DepthUserRT };
    renderer->changeRenderTargets(bufferRTs, elementsOf(bufferRTs), m_DepthRT);

    renderer->clear(false, true, false, baseColor, m_zBufferClearColor);
    renderer->clearRenderTarget(m_DepthUserRT, float4(FAR_PLANE, 0, 0, 0));

    /*
    * Main scene pass.
    * This is where the buffers are filled for the later deferred passes.
    */
    renderer->reset();
    renderer->setRasterizerState(cullBack);
    renderer->setShader(m_FillBuffers);
    renderer->setShaderConstant4x4f("ViewProj", viewProj);
    renderer->setSamplerState("Filter", m_BaseFilter);
    renderer->setDepthState(m_DepthTestCurrent);
    renderer->apply();

    // draw Map
    const uint batch_count = m_Map->getBatchCount();
    for (uint batch_id = 0; batch_id < batch_count; batch_id++)
    {
        renderer->setTexture("Base", m_BaseTex[batch_id]);
        renderer->setTexture("Bump", m_BumpTex[batch_id]);
        renderer->applyTextures();

        m_Map->drawBatch(renderer, batch_id);
    }

    // draw dynamic objects under the lights
    for (uint i = 0; i < m_LightSystem.getLightCount(); i++)
    {
        for (uint j = 0; j < SPHERES_PER_LIGHT_COUNT; j++)
        {
            renderer->setShaderConstant4x4f("ViewProj", viewProj * m_DinamicObjectInstance[i][j]->getWorld());
            renderer->applyConstants();

            m_DinamicObjectInstance[i][j]->getModel()->drawBatch(renderer, 0);
        }
    }

    // draw Chests
    for (uint i = 0; i < m_LightSystem.getLightCount(); i++)
    {
        renderer->setShaderConstant4x4f("ViewProj", viewProj * m_ChestInstance[i]->getWorld());
        renderer->applyConstants();

        renderer->setTexture("Base", m_BaseTex[1]);
        renderer->setTexture("Bump", m_BumpTex[1]);
        renderer->applyTextures();
        m_ChestInstance[i]->getModel()->drawBatch(renderer, 0);

        renderer->setTexture("Base", m_BaseTex[0]);
        renderer->setTexture("Bump", m_BumpTex[0]);
        renderer->applyTextures();
        m_ChestInstance[i]->getModel()->drawBatch(renderer, 1);
    }



    /*
    * Deferred lighting pass.
    */
    renderer->changeRenderTarget(m_lightedSceneRT); // , m_DepthRT);
    renderer->clear(true, false, false, baseColor, m_zBufferClearColor);
    renderer->reset();

    renderer->setDepthState(noDepthTest);
    renderer->setShader(m_Lighting);

    renderer->setRasterizerState(cullFront);
    renderer->setBlendState(m_BlendAdd);
    renderer->setShaderConstant4x4f("ViewProj", viewProj);
    renderer->setShaderConstant4x4f("ViewProjInv", viewProjInv * scale(1.0f / width, 1.0f / height, 1.0f));
    renderer->setShaderConstant3f("CamPos", camPos);
    renderer->setTexture("Base", m_BaseRT);
    renderer->setTexture("Normal", m_NormalRT);
    renderer->setTexture("Depth", m_DepthRT);
    renderer->apply();

    float2 zw = projection.rows[2].zw();
    for (uint i = 0; i < m_LightSystem.getLightCount(); i++)
    {
        PointLight * l = ((PointLight*) m_LightSystem.getSource(i));
        if (l == NULL) {
            continue;
        }
        float3 lightPos = l->getPosition();
        float radius = l->getRadius();
        float invRadius = 1.0f / radius;

        // Compute z-bounds
        float4 lPos = view * float4(lightPos, 1.0f);
        float z1 = lPos.z + radius;

        if (z1 > NEAR_PLANE)
        {
            float z0 = max(lPos.z - radius, NEAR_PLANE);

            float2 zBounds;
            zBounds.x = saturate(zw.x + zw.y / z0);
            zBounds.y = saturate(zw.x + zw.y / z1);
            if (m_zBufferIsInversed) {
                std::swap(zBounds.x, zBounds.y);
            }

            renderer->setShaderConstant3f("LightPos", lightPos);
            renderer->setShaderConstant1f("Radius", radius);
            renderer->setShaderConstant1f("InvRadius", invRadius);
            renderer->setShaderConstant2f("ZBounds", zBounds);
            renderer->applyConstants();

            m_lightBoundingSphere->draw(renderer);
        }
    }


    /*
    * Post effects
    */
    if (m_showDepthBuffer) {
        proceedPostProcess(m_ZBufferNormalized);
    }
    else if (m_postProcessEnabled) {
        float timeStep = (float) (getCurrentTime().QuadPart % (1000000000)) / 1.0e+6f;

        SceneInfo sceneInfo(viewProj, lightShaftsLight);
        switch (postprocessMode) {
        case LIGHT_SHAFT_OCCLUSION:
            occlusionShafts->setSceneInfo(&sceneInfo);
            occlusionShafts->setRenderTarget(FB_COLOR);
            occlusionShafts->setSettings(&lsoSettings);
            occlusionShafts->apply();
            break;

        case LIGHT_SHAFT_BLOOM:
            bloomShafts->setSceneInfo(&sceneInfo);
            bloomShafts->setRenderTarget(FB_COLOR);
            bloomShafts->setSettings(&lsbSettings);
            bloomShafts->apply();
            break;

        case LIGHT_SHAFT_GEOMETRIC:
            // draw Light Frustums
            // !!! TODO: Victor, move it to your own Effect file !!!
            renderer->changeRenderTarget(m_lightedSceneRT, m_DepthRT);
            renderer->setDepthState(noDepthWrite);

            renderer->setShader(m_GeometricLightShafts);
            renderer->setShaderConstant4x4f("ViewProj", viewProj);
            renderer->setShaderConstant4x4f("View", view);
            renderer->setShaderConstant1f("High", lsgSettings.high);
            renderer->setShaderConstant1f("Speed", lsgSettings.speed);
            renderer->setShaderConstant1f("Period", lsgSettings.period);
            renderer->setShaderConstant1i("RayCount", lsgSettings.rayCount);
            renderer->setShaderConstant1f("Time", timeStep);
            renderer->setSamplerState("Filter", m_BaseFilter);
            renderer->apply();

            for (uint i = 1; i < m_LightSystem.getLightCount(); i += 32) {
                renderer->setShaderConstant4x4f("ViewProj", viewProj * m_FrustemInstance[i]->getWorld());
                renderer->applyConstants();
                m_FrustemInstance[i]->getModel()->drawBatch(renderer, 0);
            }
            renderer->�opyRenderTarget(FB_COLOR, m_lightedSceneRT);
            break;

        default:
            renderer->�opyRenderTarget(FB_COLOR, m_lightedSceneRT);
            break;
        }
    }
    else {
        renderer->�opyRenderTarget(FB_COLOR, m_lightedSceneRT);
    }

    renderer->changeToMainFramebuffer();
}

// old method and used as an example...
void App::proceedPostProcess(ShaderID shaderId) const
{
    renderer->changeToMainFramebuffer();
    renderer->clear(true, false, false, baseColor, m_zBufferClearColor);

    renderer->reset();

    renderer->setDepthState(noDepthTest);
    renderer->setRasterizerState(cullNone);

    renderer->setShader(shaderId);

    renderer->setTexture("PreFinal", m_lightedSceneRT);
    renderer->setTexture("Depth", m_DepthRT);
    renderer->setTexture("DepthUser", m_DepthUserRT);
    renderer->setShaderConstant1f("FarPlane", FAR_PLANE);
    renderer->setShaderConstant1f("InverseDepth", m_zBufferIsInversed);

    renderer->apply();
    renderer->applyConstants();

    m_screenQuad->draw(renderer);
}

// TODO: Victor, refactor...
void App::updateFrame()
{
    float cosT[SPHERES_PER_LIGHT_COUNT];
    float sinT[SPHERES_PER_LIGHT_COUNT];
    double timeStep = (float) (getCurrentTime().LowPart) / 5.0e+6f;

    for (uint j = 0; j < SPHERES_PER_LIGHT_COUNT; j++) {
        cosT[j] = (float) cos(timeStep + M_PI_MUL_2 * float(j) / SPHERES_PER_LIGHT_COUNT);
        sinT[j] = (float) sin(timeStep + M_PI_MUL_2 * float(j) / SPHERES_PER_LIGHT_COUNT);
    }

    for (uint i = 0; i < m_LightSystem.getLightCount(); ++i) {
        PointLight *l = ((PointLight*) m_LightSystem.getSource(i));
        if (l != NULL) {
            l->setRadius(m_lightsRadius);
        }
    }

    float radius = RADIUS, yOffset = -40.f;
    if (m_postProcessEnabled && postprocessMode == LIGHT_SHAFT_GEOMETRIC) {
        radius = 40.f;
        yOffset = -80.f;
    }

    for (uint i = 0; i < m_LightSystem.getLightCount(); i++) {
        float3 lightPos = m_LightSystem.getLightPos(i);
        for (uint j = 0; j < SPHERES_PER_LIGHT_COUNT; j++) {
            m_DinamicObjectInstance[i][j]->setPos(vec3(lightPos.x + cosT[j] * radius,
                lightPos.y + yOffset,
                lightPos.z + sinT[j] * radius));
        }
    }

    float3 lightPos = m_LightSystem.getLightPos(lightShaftsLightId);
    for (uint j = 0; j < SPHERES_PER_LIGHT_COUNT; j++) {
        m_DinamicObjectInstance[lightShaftsLightId][j]->setPos(vec3(lightPos.x + cosT[j] * RADIUS - 100,
            lightPos.y - 40,
            lightPos.z + sinT[j] * RADIUS));
    }
}

void App::displayHelpText() {
    if (m_HelpDisplayTime > 0) {
        if (configMenu->isVisible()) {
            m_HelpDisplayTime = 0;
        }
        else {
            m_HelpDisplayTime -= min(frameTime, 0.1f);

            renderer->drawText("This is D3D11 Test App.\nWelcome!",
                               width * 0.5f - 220, height * 0.5f - 75, 30, 38,
                               defaultFont, linearClamp, blendSrcAlpha, noDepthTest);
        }
    }
}

/************************
* GUI changes listeners
*************************/
void App::onSliderChanged(Slider * slider) {
    if (slider == m_LightRadiusSlider) {
        m_lightsRadius = m_LightRadiusSlider->getValue();
    }

    auto sliderIt = find(lsbSettingsSliders.begin(), lsbSettingsSliders.end(), slider);
    if (sliderIt != lsbSettingsSliders.end()) {
        lsbSettings.diskRadius = lsbSettingsSliders[0]->getValue();
        lsbSettings.exposure = lsbSettingsSliders[1]->getValue();
        lsbSettings.decay = lsbSettingsSliders[2]->getValue();
        lsbSettings.density = lsbSettingsSliders[3]->getValue();
        lsbSettings.weight = lsbSettingsSliders[4]->getValue();
        lsbSettings.numSamples = static_cast<int>(lsbSettingsSliders[5]->getValue());
    }

    sliderIt = find(lsoSettingsSliders.begin(), lsoSettingsSliders.end(), slider);
    if (sliderIt != lsoSettingsSliders.end()) {
        lsoSettings.window = lsoSettingsSliders[0]->getValue();
        lsoSettings.samples = lsoSettingsSliders[1]->getValue();
        lsoSettings.factor = lsoSettingsSliders[2]->getValue();
        lsoSettings.bright = lsoSettingsSliders[3]->getValue();
        lsoSettings.fogColor = lsoSettingsSliders[4]->getValue();
        lsoSettings.fogDensity = lsoSettingsSliders[5]->getValue();
    }

    sliderIt = find(lsgSettingsSliders.begin(), lsgSettingsSliders.end(), slider);
    if (sliderIt != lsgSettingsSliders.end()) {
        lsgSettings.rayCount = (int) lsgSettingsSliders[0]->getValue();
        lsgSettings.period = lsgSettingsSliders[1]->getValue();
        lsgSettings.speed = lsgSettingsSliders[2]->getValue();
        lsgSettings.high = lsgSettingsSliders[3]->getValue();
    }

    D3D11App::onSliderChanged(slider);
}

void App::onCheckBoxClicked(CheckBox * cb) {
    if (cb == m_zBufferInversedCheckbox) {
        setZBufferInversed(cb->isChecked());
    }
    else if (cb == m_enablePostProcessCB) {
        m_postProcessEnabled = !m_postProcessEnabled;
    }
    else if (cb == m_showDepthBufferCB) {
        m_showDepthBuffer = !m_showDepthBuffer;
    }
    else if (cb == lsoDepthBufferCB) {
        lsoSettings.blurBufffer = !lsoSettings.blurBufffer;
    }
    else if (cb == lsbShowBloomCB) {
        lsbSettings.showOnlyBloom = !lsbSettings.showOnlyBloom;
    }

    D3D11App::onCheckBoxClicked(cb);
}

void App::onDropDownChanged(DropDownList* dropDownList) {
    if (dropDownList == lightShaftModeDropdown) {
        postprocessMode = itemIdToMode[dropDownList->getSelectedItem()];
        updateSettingsVisibility();
    }

    BaseApp::onDropDownChanged(dropDownList);
}

void App::onButtonClicked(Button* button) {
    if (button == resetLSBButton) {
        lsbSettings = LSBSettings();

        lsbSettingsSliders[0]->setValue(lsbSettings.diskRadius);
        lsbSettingsSliders[1]->setValue(lsbSettings.exposure);
        lsbSettingsSliders[2]->setValue(lsbSettings.decay);
        lsbSettingsSliders[3]->setValue(lsbSettings.density);
        lsbSettingsSliders[4]->setValue(lsbSettings.weight);
        lsbSettingsSliders[5]->setValue(static_cast<float>(lsbSettings.numSamples));
        lsbShowBloomCB->setChecked(lsbSettings.showOnlyBloom);
    }
    else if (button == resetLSOButton) {
        lsoSettings = LSOSettings();
        lsoSettingsSliders[0]->setValue(lsoSettings.window);
        lsoSettingsSliders[1]->setValue(lsoSettings.samples);
        lsoSettingsSliders[2]->setValue(lsoSettings.factor);
        lsoSettingsSliders[3]->setValue(lsoSettings.bright);
        lsoSettingsSliders[4]->setValue(lsoSettings.fogColor);
        lsoSettingsSliders[5]->setValue(lsoSettings.fogDensity);
        lsoDepthBufferCB->setChecked(false);
    }
    else if (button == resetLSGButton) {
        lsgSettings = LSGSettings();

        lsgSettingsSliders[0]->setValue((float) lsgSettings.rayCount);
        lsgSettingsSliders[1]->setValue(lsgSettings.period);
        lsgSettingsSliders[2]->setValue(lsgSettings.speed);
        lsgSettingsSliders[3]->setValue(lsgSettings.high);
    }
}
