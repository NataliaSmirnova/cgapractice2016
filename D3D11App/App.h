#ifndef __APP_H__
#define __APP_H__

#include "Direct3D11/D3D11App.h"
#include "Util/Model.h"
#include "LightSystem.h"
#include "LightShaftBloomEffect.h"
#include "LightShaftOcclusionEffect.h"
#include "LightShaftGeometricEffect.h"
#include <unordered_map>

using std::vector;

const uint SPHERES_PER_LIGHT_COUNT = 5;
const float M_PI_MUL_2 = 2 * 3.14159265358979323846f;
const float M_PI_DIV_2 = 3.14159265358979323846f / 2;
const float RADIUS = 30.f;

enum EffectMode
{
    NO_POST_PROCESS,
    LIGHT_SHAFT_OCCLUSION,
    LIGHT_SHAFT_BLOOM,
    LIGHT_SHAFT_GEOMETRIC
};

/*
* Main user application class, wrapped into WindowsBase.cpp (WinApi app)
*/
class App : public D3D11App /* BaseApp */
{
public:
    // Bad temporary design, many constants from app are used in effect... (redo in future)
    friend class Effect;
    friend class LightShaftOcclusionEffect;
    friend class LightShaftBloomEffect;

    App()  {

    }

    char* getTitle() const override {
        return "D3D11 Test app";
    }

    void moveCamera(const float3 &dir)  override;
    void resetCamera()                  override;

    void onSize(int w, int h)           override;
    bool onKey(const uint key, const bool pressed) override;
    bool init()     override;
    void exit()     override;
    void loadUserConfig() override;
    void updateUserConfig() override;

    bool initAPI()  override;

    bool load()     override;
    void unload()   override;
    void displayHelpText();
    void drawFrame() override;
    void proceedPostProcess(ShaderID shaderId) const;

    void createScreenPostprocessQuad();
    void initEffects();
    bool loadModels();
    void initAppGUI();

    void updateFrame();
    void setZBufferInversed(bool inversed = true);

    /* GUI widgets handlers */
    void onSliderChanged(Slider *slider) override;
    void onCheckBoxClicked(CheckBox* cb) override;
    void onDropDownChanged(DropDownList *dropDownList) override;
    void onButtonClicked(Button *button) override;

    /* Add GUI widget shortcut function */
    template <class W>
    void addWidget(W* widget) {
        widget->setListener(this);
        widgets.addFirst(widget);
    }
    
    void addWidget(Dialog* widget) {
        widgets.addFirst(widget);
    }

    void initDinamicObjects(uint numberOfGroups, uint numberOfObjectsInGroup);
    bool initLights(); // Lights initialization
    void updateSettingsVisibility();
    
protected:
    /* Resources */
    ShaderID m_FillBuffers,
             m_GeometricLightShafts,
             m_Lighting,
             m_ZBufferNormalized,
             m_RenderDownsampled,
             m_Ņonvolution1D;


    TextureID m_BaseTex[5], m_BumpTex[5];
    TextureID m_BaseRT, 
              m_NormalRT,
              m_TempRT,
              m_TempRT2,
              m_HalfScreenRT,
              m_DepthUserRT,            // user generated z-buffer (in pixel shader)
              m_DepthRT,                // DirectX generated z-buffer (logarithmic, 0..1)
              m_lightedSceneRT;         // final image without post processing applied

    SamplerStateID m_BaseFilter, m_PointClamp;
    BlendStateID m_BlendAdd, m_BlendDecal;
    DepthStateID m_DepthTest, m_DepthTestInversed, m_DepthTestCurrent;


    /* Models */
    Model *m_Map;
    Model *m_lightBoundingSphere;
    Model *m_screenQuad;

    Model *m_DinamicObject;
    DinamicModelInstance ***m_DinamicObjectInstance;
    Model *m_Chest;
    DinamicModelInstance **m_ChestInstance;
    Model *m_Frustem;
    DinamicModelInstance **m_FrustemInstance;
    uint m_numberOfGroups;
    uint m_numberOfObjectsInGroup;


    /* Effects */
    LightShaftOcclusionEffect *occlusionShafts = nullptr;
    LSOSettings lsoSettings;

    LightShaftBloomEffect *bloomShafts = nullptr;
    LSBSettings lsbSettings;
    int downSampleRate = 4;

    LightShaftGeometricEffect *geometricShafts = nullptr;
    LSGSettings lsgSettings;


    /* GUI widgets  */
    Slider *m_LightRadiusSlider;
    CheckBox *m_zBufferInversedCheckbox;
    CheckBox *m_enablePostProcessCB;
    CheckBox *m_showDepthBufferCB;


    // Effects settings dialog
    Dialog *lsbSettingsGroup;
    vector<Slider*> lsbSettingsSliders;
    CheckBox *lsbShowBloomCB;
    Button *resetLSBButton;

    Dialog *lsoSettingsGroup;
    vector<Slider*> lsoSettingsSliders;
    CheckBox *lsoDepthBufferCB;
    Button *resetLSOButton;

    Dialog *lsgSettingsGroup;
    vector<Slider*> lsgSettingsSliders;
    Button *resetLSGButton;
    
    // Effects switch
    DropDownList *lightShaftModeDropdown;
    std::unordered_map<int, EffectMode> itemIdToMode; // map switch element id to EfectMode


    /* Lighting */
    LightSystem m_LightSystem;
    PointLight *lightShaftsLight; // test light for light shaft methods (close to camera)
    uint lightShaftsLightId;


    /* Other */
    float m_lightsRadius = 700;
    bool m_postProcessEnabled = true;
    bool m_showDepthBuffer = false;
    bool m_showBloomEffect = true;
    bool m_showOcclusionEffect = true;

    // NOTE: DirectX-generated z-buffer is nor linear distrubuted, but log() distributed
    // normal z-buffer:   0-near, 1-far
    // inversed z-buffer: 1-near, 0-far (NO visual changes, just DirectX internal parameters changes)
    bool m_zBufferIsInversed = false;
    float m_zBufferClearColor = 1.0f; // color of farthest point in scene

    float m_HelpDisplayTime;

    float baseColor[4];
    const float NEAR_PLANE  = 20.0f;
    const float FAR_PLANE   = 12000.0f;

    EffectMode postprocessMode = LIGHT_SHAFT_BLOOM;
};



#endif