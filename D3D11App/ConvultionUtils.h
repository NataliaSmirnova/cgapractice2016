#pragma once

#include "Math/Vector.h""

namespace utils {
	void generateOffsetsX(float2 *offsets, int radius) {
		for (int i = 0; i < (2 * radius + 1); ++i) {
			offsets[i] = float2((i - radius), 0);
		}
	}

	void generateOffsetsY(float2 *offsets, int radius) {
		for (int i = 0; i < (2 * radius + 1); ++i) {
			offsets[i] = float2(0, (i - radius));
		}
	}

	void generateGaussWeights(float *weights, int radius, float sigma = 2) {
		float  x, coeff, sum = 0.0f;
		float kernelSize = (2 * radius + 1);

		for (int i = 0; i < kernelSize; ++i) {
			x = float(i - radius);
			sum += coeff = 1 / (sigma * sqrt(2 * PI)) * exp(-x*x / (2 * sigma));
			weights[i] = coeff;
		}

		for (int i = 0; i < kernelSize; ++i) {
			weights[i] /= sum;
		}
	}
}