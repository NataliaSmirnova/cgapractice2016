﻿#include "Effect.h"

Effect::Effect(Renderer *renderer, App *app)
{
    this->renderer = renderer;
    this->app = app;
    this->targetTextureID = -100500;
}

void Effect::setRenderTarget(TextureID textureId)
{
    targetTextureID = textureId;
}

void Effect::setSettings(EffectSettings *settings)
{
    this->settings = settings;
}

void Effect::setSceneInfo(SceneInfo *sceneInfo)
{
    this->sceneInfo = sceneInfo;
}
