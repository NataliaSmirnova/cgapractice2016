﻿#ifndef __EFFECT_HH__
#define __EFFECT_HH__

#include "Math/Vector.h"
#include "LightSystem.h"

class App;
class Renderer;

/*
* Base abstract struct for effect settings
*/
struct EffectSettings {
    virtual ~EffectSettings() = 0 {};
};


/*
* Structure contains data needed for some effect, but that does not relate to effect settings directly
*/
struct SceneInfo {
    float4x4 viewProj;
    PointLight *light;

    SceneInfo(float4x4 viewProj, PointLight *light) {
        this->viewProj = viewProj;
        this->light = light;
    }
};


/*
* Base abstract class for some effect.
* Effect can be a wrap for shader and should contain all resources (models, textures, shaders)
* that are relevant to the effect
*/
class Effect
{
public:
    Effect(Renderer *_renderer, App *_app);
    virtual ~Effect() {};

    virtual int init() = 0;
    virtual void apply() = 0;

    void setRenderTarget(TextureID textureId);
    void setSettings(EffectSettings *settings);
    void setSceneInfo(SceneInfo *sceneInfo);

protected:
    TextureID targetTextureID = -1;
    EffectSettings *settings;
    SceneInfo *sceneInfo;

    Renderer *renderer;
    App *app;   /* TEMP design decision
                   access to all app members (Effect is friend of App)
                   this is done because right now i cant create determine common interface for all Effects
                   in future after practice it will be determined. 
                   Ideally, some global Resource Manager should be created. */
};

#endif