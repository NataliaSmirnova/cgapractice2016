#include "Light.h"

LightSystem::LightSystem(int lights_number) : lightCount(lights_number)
{
    if (lights_number) {
        mLights = new AbstractLightSource*[lights_number];
        isEnabled = new bool[lights_number];
        for (int i = 0; i < lights_number; ++i) {
            mLights[i] = NULL;
            isEnabled[i] = true;
        }
    }
}

LightSystem::~LightSystem()
{
    for (int i = 0; i < lightCount; ++i) {
        delete mLights[i];
    }
    delete[] mLights;
    delete[] isEnabled;
}

void LightSystem::setSource(AbstractLightSource * src, int index, bool active)
{
    if (isValid(index)) {
        if (mLights[index] != NULL) {
            delete mLights[index];
        }
        mLights[index] = src;
        isEnabled[index] = active;
    }
}

AbstractLightSource* LightSystem::getSource(int index)
{
    return (isValid(index) && (fullMode || isEnabled[index]) ? mLights[index] : NULL);
}

float3& LightSystem::getLightPos(int index)
{
    return (isValid(index) ? mLights[index]->getPosition() : NULL);
}

void LightSystem::removeSource(int index)
{
    if (isValid(index) && mLights[index] != NULL) {
        delete mLights[index];
        mLights[index] = NULL;
        isEnabled[index] = false;
    }
}

void LightSystem::setShaderPath(char * str)
{
    shaderPath = str;
}

const char* LightSystem::getShaderPath()
{
    return shaderPath.c_str();
}

void LightSystem::resetLightCount(int lights_number)
{
    if (lightCount) {
        for (int i = 0; i < lightCount; ++i) {
            delete mLights[i];
        }
        delete[] mLights;
        delete[] isEnabled;
    }
    lightCount = lights_number;
    mLights = new AbstractLightSource*[lights_number];
    isEnabled = new bool[lights_number];
    for (int i = 0; i < lights_number; ++i) {
        mLights[i] = NULL;
        isEnabled[i] = false;
    }
}

void LightSystem::setFullMode(bool full)
{
    fullMode = full;
}

bool LightSystem::isValid(int index)
{
    return ((index >= 0) && (index < lightCount));
}