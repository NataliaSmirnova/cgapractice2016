﻿#include "LightShaftBloomEffect.h"
#include "App.h"
#include "utils.h"

LightShaftBloomEffect::~LightShaftBloomEffect() {
    delete lightSphere;
}

int LightShaftBloomEffect::init()
{
    if ((firstPassShader = renderer->addShader("Shaders/LightDraw.shd")) == SHADER_NONE) return -1;
    if ((secondPassShader = renderer->addShader("Shaders/LSBloom.shd")) == SHADER_NONE) return -1;

    lightSphere = new Model(); // create model for drawing light disk
    lightSphere->createSphere(3);
    if (!lightSphere->makeDrawable(renderer, true, firstPassShader))  return -1;

    return 0;
}


void LightShaftBloomEffect::apply() {

    LSBSettings &s = *(LSBSettings *) settings;

    // 1) First pass, draw light as white disk
    renderer->changeRenderTarget(app->m_TempRT, app->m_DepthRT);
    renderer->clear(true, false, false, app->baseColor, app->m_zBufferClearColor);

    renderer->reset();

    renderer->setDepthState(app->m_DepthTestCurrent);
    renderer->setRasterizerState(app->cullBack);
    renderer->setBlendState(app->m_BlendAdd);

    renderer->setShader(firstPassShader);

    renderer->setShaderConstant4x4f("ViewProj", sceneInfo->viewProj);
    renderer->setShaderConstant3f("LightPos", sceneInfo->light->getPosition());
    renderer->setShaderConstant1f("Radius", s.diskRadius);
    renderer->setShaderConstant4f("LightColor", float4(1, 1, 1, 1));

    renderer->apply();
    renderer->applyConstants();

    lightSphere->draw(renderer);


    // 2) Second pass, blur light... (down sampled)
    renderer->changeRenderTarget(app->m_HalfScreenRT);
    renderer->clear(true, false, false, app->baseColor, app->m_zBufferClearColor);

    renderer->reset();

    renderer->setDepthState(app->noDepthTest);
    renderer->setRasterizerState(app->cullNone);
    renderer->setBlendState(app->m_BlendAdd); // maybe change

    renderer->setShader(secondPassShader);

    renderer->setTexture("FirstLightPass", app->m_TempRT);
    renderer->setShaderConstant1f("Exposure",   s.exposure);
    renderer->setShaderConstant1f("Decay",      s.decay);
    renderer->setShaderConstant1f("Density",    s.density);
    renderer->setShaderConstant1f("Weight",     s.weight);
    renderer->setShaderConstant1i("NumSamples", s.numSamples);
    renderer->setShaderConstant1i("DownSampleRate", app->downSampleRate);
    
    float2 lightScreenPos = utils::getPointScreenPos(sceneInfo->light->getPosition(), 
                                                     sceneInfo->viewProj, float(app->width), float(app->height));
    renderer->setShaderConstant2f("LightPositionOnScreen", lightScreenPos);
    renderer->apply();
    renderer->applyConstants();

    app->m_screenQuad->draw(renderer);


    // 3) Draw down sampled texture to original size texture
    renderer->changeRenderTarget(app->m_TempRT2);
    renderer->clear(true, false, false, app->baseColor, app->m_zBufferClearColor);

    renderer->setShader(app->m_RenderDownsampled);
    renderer->setTexture("DownsampledTexture", app->m_HalfScreenRT);
    renderer->setSamplerState("Filter", app->m_BaseFilter);
    renderer->apply();
    renderer->applyConstants();

    app->m_screenQuad->draw(renderer);


    blurDownsampled = false; // for debug
    if (blurDownsampled) {
        renderer->сopyRenderTarget(targetTextureID, app->m_TempRT2);
        return;
    }


    // 4) Blur due to down sampling
    const int RADIUS = 3;
    const int KERNEL_SIZE = RADIUS * 2 + 1;
    // array constants  are kept as float4 for fast access in GPU due to packing by 16 bytes (!float4) 
    // for details see https://geidav.wordpress.com/2013/03/05/hidden-hlsl-performance-hit-accessing-unpadded-arrays-in-constant-buffers/
    float4 offsets[KERNEL_SIZE]; 
    float4 weights[KERNEL_SIZE];
    utils::generateOffsetsX(offsets, RADIUS);
    utils::generateGaussWeights(weights, RADIUS);

    renderer->setBlendState(app->m_BlendAdd); // maybe change
    renderer->setShader(app->m_Сonvolution1D);

    // horizontal blur
    renderer->changeRenderTarget(app->m_TempRT);
    renderer->clear(true, false, false, app->baseColor, app->m_zBufferClearColor);

    renderer->setTexture("Image", app->m_TempRT2);
    renderer->setShaderConstantArray4f("offsets", offsets, KERNEL_SIZE);
    renderer->setShaderConstantArray4f("weights", weights, KERNEL_SIZE);
    renderer->setShaderConstant1f("radius", float(RADIUS));

    renderer->apply();
    renderer->applyConstants();


    app->m_screenQuad->draw(renderer);

    // vertical blur
    utils::generateOffsetsY(offsets, RADIUS);
    renderer->changeRenderTarget(targetTextureID);
    renderer->clear(true, false, false, app->baseColor, app->m_zBufferClearColor);

    renderer->setTexture("Image", app->m_TempRT);
    renderer->setShaderConstantArray4f("offsets", offsets, KERNEL_SIZE);
    renderer->apply();
    renderer->applyConstants();

    if (!s.showOnlyBloom) {
        renderer->сopyRenderTarget(targetTextureID, app->m_lightedSceneRT);
    }

    app->m_screenQuad->draw(renderer);
}