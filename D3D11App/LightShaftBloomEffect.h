﻿#ifndef __LS_BLOOM_H__
#define __LS_BLOOM_H__

#include "Effect.h"
#include <Renderer.h>
#include <Util/Model.h>
#include "LightSystem.h"

// Bloom Light shaft technique settings that can be manipulated through GUI menu
struct LSBSettings : public EffectSettings
{
    float diskRadius = 34;  // light disk radius (sphere) in world coordinate system
    float exposure = 0.18f; // result multiplication constant
    float decay = 0.987f;   // how fast the impact of the pixel along the ray to the light center is decreasing
    float density = 1.0f;   // portion (0..1) of the ray to sample pixels in (sample along full ray / half / third...)
    float weight = 0.25;    // weight of each pixel (constant)
    int numSamples = 140;   // number of pixels to sample along the ray
    bool showOnlyBloom = false; // whether to show only blurred disk without applying it to rendered scene
};

class LightShaftBloomEffect : public Effect
{
public:
    LightShaftBloomEffect(Renderer* _renderer, App* _app)
        : Effect(_renderer, _app) {}
    ~LightShaftBloomEffect();

    int init() override;
    void apply() override;


private:
    ShaderID firstPassShader;   // draw white light disk to black background  using z-buffer of rendered scene
    ShaderID secondPassShader;  // blur light disk away from light position

    float lightDiskRadius = 20.f;
    Model *lightSphere = nullptr;   // model for drawing light disk

    float2 LightShaftBloomEffect::getPointScreenPos(float3 &point, float4x4 &viewProj, float width, float height) const;

    bool blurDownsampled = true;
};



#endif
