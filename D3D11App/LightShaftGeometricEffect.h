﻿#ifndef __LS_GEOMETRIC_H__
#define __LS_GEOMETRIC_H__

#include "Effect.h"
#include <Renderer.h>
#include "LightSystem.h"

struct LSGSettings : public EffectSettings
{
    int rayCount = 21;
    float high = 0.2f;
    float speed = 0.5f;
    float period = 0.2f;
};

class LightShaftGeometricEffect : public Effect
{
public:
    LightShaftGeometricEffect(Renderer* _renderer, App* _app)
        : Effect(_renderer, _app) {}
    ~LightShaftGeometricEffect();

    int init() override;
    void apply() override;
private:
    ShaderID firstPass;
    ShaderID secondPass;
};



#endif
