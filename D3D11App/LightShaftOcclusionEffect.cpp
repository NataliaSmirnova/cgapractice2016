﻿#include "LightShaftOcclusionEffect.h"
#include "App.h"
#include "utils.h"

//Loading shaders for 1st and 2nd pass
int LightShaftOcclusionEffect::init()
{
    if ((firstPass = renderer->addShader("Shaders/BlurredDepth.shd")) == SHADER_NONE) return -1;
    if ((secondPass = renderer->addShader("Shaders/LSOcclusion.shd")) == SHADER_NONE) return -1;
    return 0;
}

void LightShaftOcclusionEffect::apply()
{
    LSOSettings &s = *(LSOSettings *) settings;

    //Check if blurred depth buffer or full effect
    if (s.blurBufffer) {
        renderer->changeToMainFramebuffer();
    } else {
        renderer->changeRenderTarget(app->m_TempRT, app->m_DepthRT);
    }

    renderer->clear(true, false, false, app->baseColor, app->m_zBufferClearColor);
    renderer->reset();

    renderer->setDepthState(app->noDepthTest);
    renderer->setRasterizerState(app->cullNone);

    //Blur depth buffer
    renderer->setShader(firstPass);

    float2 lightScreenPos = utils::getPointScreenPos(sceneInfo->light->getPosition(), 
                                                     sceneInfo->viewProj, float(app->width), float(app->height));

    renderer->setShaderConstant2f("LightPos",  lightScreenPos);
    renderer->setTexture("DepthUser", app->m_DepthUserRT);
    renderer->setShaderConstant1f("FarPlane", app->FAR_PLANE);
    renderer->setShaderConstant1f("samples", s.samples);
    renderer->setShaderConstant1f("window", s.window);
    renderer->setShaderConstant1f("blur_factor", s.factor);
    renderer->setShaderConstant1f("blur_bright", s.bright);
    renderer->setShaderConstant1f("w", (float) app->getWidth());
    renderer->setShaderConstant1f("h", (float) app->getHeight());
    renderer->apply();
    renderer->applyConstants();

    app->m_screenQuad->draw(renderer);

    //Z-based fog
    if (!s.blurBufffer) {
        renderer->changeRenderTarget(targetTextureID);
        renderer->clear(true, false, false, app->baseColor, app->m_zBufferClearColor);
        renderer->reset();

        renderer->setDepthState(app->m_DepthTestCurrent);
        renderer->setRasterizerState(app->cullNone);
        renderer->setBlendState(app->m_BlendAdd);

        renderer->setShader(secondPass);

        renderer->setShaderConstant2f("LightPos", lightScreenPos);
        renderer->setTexture("DepthUser", app->m_TempRT);
        renderer->setTexture("firstPass", app->m_lightedSceneRT);
        renderer->setShaderConstant1f("fogDensity", s.fogDensity);
        renderer->setShaderConstant4f("fogColor", float4(s.fogColor, s.fogColor, s.fogColor, s.fogColor));
        renderer->setShaderConstant1f("FarPlane", app->FAR_PLANE);
        renderer->apply();
        renderer->applyConstants();

        app->m_screenQuad->draw(renderer);
    }
}


LightShaftOcclusionEffect::~LightShaftOcclusionEffect()
{

}
