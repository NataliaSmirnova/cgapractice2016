﻿#ifndef __LS_OCCLUSION_H__
#define __LS_OCCLUSION_H__

#include "Effect.h"
#include <Renderer.h>
#include "LightSystem.h"

//Light shaft technique settings that can be manipulated through GUI menu
struct LSOSettings : public EffectSettings
{
    float window = 50.0f;
    float samples = 80.0f;
    float factor = 1.0f;
    float bright = 5.0f;
    float fogDensity = 1.0f;
    float fogColor = 1.0f;
    bool blurBufffer = false;
};

//Light shaft occlusion effect class
class LightShaftOcclusionEffect : public Effect
{
public:
    LightShaftOcclusionEffect(Renderer* _renderer, App* _app)
        : Effect(_renderer, _app) {}
    ~LightShaftOcclusionEffect();

    int init() override;
    void apply() override;
private:
    ShaderID firstPass;
    ShaderID secondPass;

};



#endif
