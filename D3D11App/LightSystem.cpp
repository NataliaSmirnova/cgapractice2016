#include "LightSystem.h"

LightSystem::LightSystem(int lights_number)
{
    if (lights_number) {
        mLights.resize(lights_number);
    }
}

LightSystem::~LightSystem()
{
    for (uint i = 0; i < mLights.size(); ++i) {
        delete mLights[i];
    }
}

void LightSystem::setSource(AbstractLightSource * src, int index, bool active)
{
    if (isValid(index)) {
        if (mLights[index] != NULL) {
            delete mLights[index];
        }
        mLights[index] = src;
    }
}

int LightSystem::addSource(AbstractLightSource * src, bool active)
{
    src->setEnabled(active);
    mLights.push_back(src);
    return mLights.size() - 1;
}

AbstractLightSource* LightSystem::getSource(int index)
{
    return (isValid(index) && (fullMode || mLights[index]->isEnabled()) ? mLights[index] : NULL);
}

float3 LightSystem::getLightPos(int index)
{
    return (isValid(index) ? mLights[index]->getPosition() : float3(0.0f, 0.0f, 0.0f));
}

void LightSystem::removeSource(int index)
{
    if (isValid(index) && mLights[index] != NULL) {
        delete mLights[index];
        mLights[index] = NULL;
    }
}

void LightSystem::setShaderPath(char * str)
{
    shaderPath = str;
}

const char* LightSystem::getShaderPath()
{
    return shaderPath.c_str();
}

void LightSystem::resetLightCount(int lights_number)
{
    for (uint i = 0; i < mLights.size(); ++i) {
        delete mLights[i];
    }
    mLights.clear();
    mLights.resize(lights_number);
}

void LightSystem::setFullMode(bool full)
{
    fullMode = full;
}

bool LightSystem::isValid(int index)
{
    return ((index >= 0) && (static_cast<uint>(index) < mLights.size()));
}