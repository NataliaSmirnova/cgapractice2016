#ifndef LIGHT_H_INCLUDED
#define LIGHT_H_INCLUDED
#include "BaseApp.h"
#include <string>
#include <vector>


/*
* Light source hierarchy will allow to use different types of lights
*/
class AbstractLightSource
{
public:
    AbstractLightSource() : pos(float3(0.0f, 0.0f, 0.0f)), m_isEnabled(true) { };
    AbstractLightSource(float3 init_pos) : pos(init_pos) {};
    AbstractLightSource(float3 init_pos, bool enabled) : pos(init_pos), m_isEnabled(true) {};
    virtual ~AbstractLightSource() = 0 {};

    float3 getPosition() {
        return pos;
    };
    void setPosition(float3 set_pos) {
        pos = set_pos;
        mWorld = rotateXY(0, 0);
        mWorld.translate(pos);
    };

    float4x4 getWorld() {
        return mWorld;
    }

    bool isEnabled() const {
        return m_isEnabled;
    }
    void setEnabled(bool val) {
        m_isEnabled = val;
    }

protected:
    float3 pos;
    bool m_isEnabled;
    float4x4 mWorld;
};

class PointLight : public AbstractLightSource
{
public:
    PointLight() : AbstractLightSource(float3(0.0f, 0.0f, 0.0f)), radius(1.0f) { };
    PointLight(float3 init_pos, float r) : AbstractLightSource(init_pos), radius(r) {};
    virtual ~PointLight() {};

    float getRadius() {
        return radius;
    };
    void setRadius(float set_r) {
        radius = set_r;
    };

private:
    float radius;
};


/*
* Light class will contain most things about light in scene
*/
class LightSystem
{
public:
    LightSystem(int lights_number = 0);
    ~LightSystem();

    AbstractLightSource * getSource(int index);
    void setSource(AbstractLightSource * src, int index, bool active);
    int addSource(AbstractLightSource * src, bool active);
    void removeSource(int index);

    float3 LightSystem::getLightPos(int index);
    void setShaderPath(char *str);
    const char *getShaderPath();

    void resetLightCount(int lights_number);
    void setFullMode(bool full);

    uint getLightCount() {
        return mLights.size();
    }

private:
    std::vector<AbstractLightSource*> mLights;
    bool fullMode;
    std::string shaderPath;
    bool isValid(int index);
};


#endif