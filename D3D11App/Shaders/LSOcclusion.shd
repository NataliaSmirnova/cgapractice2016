struct VsIn
{
    float4 position : Position;
};

struct PsIn
{
    float4 position : SV_Position;
};

[Vertex shader]

PsIn main(VsIn In)
{
    PsIn Out;
    Out.position = In.position;
    return Out;
}

[Fragment shader]

float2 LightPos;                           //Light screen position
Texture2D <float> DepthUser;               //DepthBuffer in Texture
Texture2D <float4> firstPass;              //Lighted scene
float fogDensity;                          //Density of fog
float3 fogColor;                           //Fog color
float FarPlane;                            //FarPlane

float3 main(PsIn In) : SV_Target
{
    float3 color;
    int3 texCoord = int3(int2(In.position.xy), 0);
    int3 lpCoord  = int3(int2(LightPos.xy), 0);
    float3 sample = firstPass.Load(texCoord);
    float z = DepthUser.Load(texCoord);
    float s = 1.0f / exp((z * fogDensity)*(z * fogDensity));
    color = sample * s + fogColor * (1 - s);
    return color;
}
