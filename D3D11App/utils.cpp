﻿#include "utils.h"


void utils::generateOffsetsX(float2 *offsets, int radius)
{
    iterateForRadius(radius, [&](int i) -> void {
        offsets[i] = float2(float((i - radius)), 0);
    });
}

void utils::generateOffsetsY(float4 *offsets, int radius)
{
    iterateForRadius(radius, [&](int i) -> void {
        offsets[i] = float4(float(i - radius), 0, 0, 0);
    });
}

void utils::generateOffsetsY(float2 *offsets, int radius)
{
    iterateForRadius(radius, [&](int i) -> void {
        offsets[i] = float2(0, float(i - radius));
    });
}

void utils::generateOffsetsX(float4 *offsets, int radius)
{
    iterateForRadius(radius, [&](int i) -> void {
        offsets[i] = float4(0, float(i - radius), 0, 0);
    });
}

void utils::generateGaussWeights(float *weights, int radius, float sigma /*= 2*/)
{
    float  x, coeff, sum = 0.0f;
    float kernelSize = float(2 * radius + 1);

    iterateForRadius(radius, [&](int i) -> void {
        x = float(i - radius);
        sum += coeff = 1 / (sigma * sqrt(2 * PI)) * exp(-x*x / (2 * sigma));
        weights[i] = coeff;
    });

    iterateForRadius(radius, [&](int i) -> void {
        weights[i] /= sum;
    });
}

void utils::generateGaussWeights(float4 *weights, int radius, float sigma /*= 2*/)
{
    float  x, coeff, sum = 0.0f;
    float kernelSize = float(2 * radius + 1);

    iterateForRadius(radius, [&](int i) -> void {
        x = float(i - radius);
        sum += coeff = 1 / (sigma * sqrt(2 * PI)) * exp(-x*x / (2 * sigma));
        weights[i] = float4(coeff, 0, 0, 0);
    });

    iterateForRadius(radius, [&](int i) -> void {
        weights[i] /= sum;
    });
}

float2 utils::getPointScreenPos(float3 &point, float4x4 &viewProj, float width, float height)
{
    float4 pos = float4(point, 1.0f);
    float4 lightScreenPos = (viewProj * pos);
    lightScreenPos *= 1.0f / lightScreenPos.w;
    return float2(0.5f * width * (1 + lightScreenPos.x),
                  0.5f * height * (1 - lightScreenPos.y));
}

void utils::iterateForRadius(int radius, std::function<void(int)> func)
{
    int kernelSize = int(2 * radius + 1);
    for (int i = 0; i < kernelSize; ++i) {
        func(i);
    }
}

