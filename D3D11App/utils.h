#ifndef  _UTILS_H_
#define _UTILS_H_

#include "Math/Vector.h"
#include "Util/Model.h"
#include <functional>
#include <string>

namespace utils {
    void generateOffsetsX(float2 *offsets, int radius);
    void generateOffsetsY(float2 *offsets, int radius);
    void generateOffsetsX(float4 *offsets, int radius);
    void generateOffsetsY(float4 *offsets, int radius);
    void generateGaussWeights(float *weights, int radius, float sigma = 2);
    void generateGaussWeights(float4 *weights, int radius, float sigma = 2);

    float2 getPointScreenPos(float3 &point, float4x4 &viewProj, float width, float height);

    void iterateForRadius(int radius, std::function<void (int)> func);;

    
    struct ModeFastInit {
        Model** ppModel;
        std::string path;
        float3 scale;
        ModeFastInit(Model **_ppModel, std::string _path, float3 _scale = float3(1.0f, 1.0f, 1.0f))
            : ppModel(_ppModel), path(_path), scale(_scale) {};
    };
}


#endif