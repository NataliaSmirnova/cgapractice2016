
/* * * * * * * * * * * * * Author's note * * * * * * * * * * * *\
*   _       _   _       _   _       _   _       _     _ _ _ _   *
*  |_|     |_| |_|     |_| |_|_   _|_| |_|     |_|  _|_|_|_|_|  *
*  |_|_ _ _|_| |_|     |_| |_|_|_|_|_| |_|     |_| |_|_ _ _     *
*  |_|_|_|_|_| |_|     |_| |_| |_| |_| |_|     |_|   |_|_|_|_   *
*  |_|     |_| |_|_ _ _|_| |_|     |_| |_|_ _ _|_|  _ _ _ _|_|  *
*  |_|     |_|   |_|_|_|   |_|     |_|   |_|_|_|   |_|_|_|_|    *
*                                                               *
*                     http://www.humus.name                     *
*                                                                *
* This file is a part of the work done by Humus. You are free to   *
* use the code in any way you like, modified, unmodified or copied   *
* into your own work. However, I expect you to respect these points:  *
*  - If you use this file and its contents unmodified, or use a major *
*    part of this file, please credit the author and leave this note. *
*  - For use in anything commercial, please request my approval.     *
*  - Share your work and ideas too as much as you can.             *
*                                                                *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef _CONFIG_H_
#define _CONFIG_H_

#include "Platform.h"
#include "Util/Array.h"
#include <sstream>
#include <memory>

struct Entry {
    //typedef std::shared_ptr<BYTE> ByteDataType;
    typedef char* ByteDataType;

    Entry(const char *eName, const int val, const bool dirt): byteData(nullptr) {
        name = new char[strlen(eName) + 1];
        strcpy(name, eName);
        value = val;
        dirty = dirt;
    }
    Entry(const char *eName, const float val, const bool dirt): byteData(nullptr) {
        name = new char[strlen(eName) + 1];
        strcpy(name, eName);
        floatValue = val;
        dirty = dirt;
    }
    Entry(const char *eName, char *b, size_t dataSize, const bool dirt) {
        name = new char[strlen(eName) + 1];
        strcpy(name, eName);
        byteData = b;
        byteDataSize = dataSize;
        dirty = dirt;
    }

    char *name;
    union {
        int value;
        float floatValue;
    };

    bool dirty;

    ByteDataType byteData;
    size_t byteDataSize = 0;
};


/*
* Holds app config parameters (Fullscreen, WindowedWidth, ShowFPS, etc...)
* Saves config in registry 'HKEY_CURRENT_USER\\Software\\Humus' (Windows)
*/
class Config {
public:
    Config();
    ~Config();

    bool init(const char *subPath = NULL);
    bool flush();

    bool getBoolDef(const char *name, const bool def) const;
    int getIntegerDef(const char *name, const int def) const;
    float getFloatDef(const char *name, const float def) const;
    bool getInteger(const char *name, int &dest) const;

    void setBool(const char *name, const bool val);
    void setInteger(const char *name, const int val);
    void setFloat(const char *name, const float val);

    template <typename T>
    void setObject(const char *name, const T &structData) {
        size_t structSize = sizeof(T);
        //char *data = new char(structSize);
        char *data = static_cast<char*>(malloc(sizeof(T)));
        memcpy(data, &structData, structSize);

        for (unsigned int i = 0; i < entries.getCount(); i++) {
            if (stricmp(entries[i].name, name) == 0) {
                if (entries[i].byteDataSize)
                    free(entries[i].byteData);
                entries[i].byteData = data;
                entries[i].byteDataSize = structSize;
                entries[i].dirty = true;
                return;
            }
        }

        entries.add(Entry(name, data, structSize, true));
    }

    char* getObject(const char *name) {
        for (unsigned int i = 0; i < entries.getCount(); i++) {
            if (stricmp(entries[i].name, name) == 0) {
                return entries[i].byteData;
            }
        }
        return nullptr;
    }

    static const size_t MAX_STRUCT_SIZE = 1024;

private:
    Array <Entry> entries;
    char *path;
};


#endif // _CONFIG_H_
