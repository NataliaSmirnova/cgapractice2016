
/* * * * * * * * * * * * * Author's note * * * * * * * * * * * *\
*   _       _   _       _   _       _   _       _     _ _ _ _   *
*  |_|     |_| |_|     |_| |_|_   _|_| |_|     |_|  _|_|_|_|_|  *
*  |_|_ _ _|_| |_|     |_| |_|_|_|_|_| |_|     |_| |_|_ _ _     *
*  |_|_|_|_|_| |_|     |_| |_| |_| |_| |_|     |_|   |_|_|_|_   *
*  |_|     |_| |_|_ _ _|_| |_|     |_| |_|_ _ _|_|  _ _ _ _|_|  *
*  |_|     |_|   |_|_|_|   |_|     |_|   |_|_|_|   |_|_|_|_|    *
*                                                               *
*                     http://www.humus.name                     *
*                                                                *
* This file is a part of the work done by Humus. You are free to   *
* use the code in any way you like, modified, unmodified or copied   *
* into your own work. However, I expect you to respect these points:  *
*  - If you use this file and its contents unmodified, or use a major *
*    part of this file, please credit the author and leave this note. *
*  - For use in anything commercial, please request my approval.     *
*  - Share your work and ideas too as much as you can.             *
*                                                                *
\* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

#ifndef _D3D11APP_H_
#define _D3D11APP_H_

#include "Direct3D11Renderer.h"
#include "../BaseApp.h"

// InitAPI flags
#define NO_SETTING_CHANGE 0x1
#define SAMPLE_BACKBUFFER 0x2

/*
* Additional hierarchy layer between BaseApp and user application
* introducing DirectX Api
*/
class D3D11App : public BaseApp
{
public:
  D3D11App();

  bool initCaps() override;
  bool initAPI() override;
  void exitAPI() override;

  void beginFrame() override;
  void endFrame() override;

  void onSize(int w, int h) override;

  bool captureScreenshot(Image &img) override;

protected:
  enum API_Revision
  {
    D3D11,
    D3D10_1,
    D3D10_0,
  };


  bool initAPI(API_Revision api_revision, 
               DXGI_FORMAT backBufferFmt, 
               DXGI_FORMAT depthBufferFmt, 
               int samples, 
               uint flags);

  bool createBuffers(bool sampleBackBuffer);
  bool deleteBuffers();

  ID3D11Device        *device;
  ID3D11DeviceContext *context;

  IDXGISwapChain      *swapChain;

  ID3D11Texture2D     *backBuffer;
  ID3D11Texture2D     *depthBuffer;
  ID3D11RenderTargetView *backBufferRTV;
  ID3D11DepthStencilView *depthBufferDSV;

  TextureID   backBufferTexture;

  DXGI_FORMAT backBufferFormat;
  DXGI_FORMAT depthBufferFormat;

  int msaaSamples;
};

#endif // _D3D11APP_H_
